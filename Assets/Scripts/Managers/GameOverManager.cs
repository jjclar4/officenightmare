﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public PlayerHealth playerHealth;


   
  private GameObject endText;

    Animator anim;
    Text text;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger("GameOver");
            endText = GameObject.Find("GameOverText");
            text = endText.GetComponentInChildren<Text>();
            text.text = "Game Over!";
            ScoreManager.ENDGAME = true;
        }
        else if (ScoreManager.SO2Complete == true)
        {
            anim.SetTrigger("ProjectComplete");
            endText = GameObject.Find("GameOverText");
            text = endText.GetComponentInChildren<Text>();
            text.text = "Project Complete!";
            ScoreManager.ENDGAME = true;
        }
    }
}
