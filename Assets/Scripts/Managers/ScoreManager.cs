﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    public static int BRDCount = 0;
    public static int BRDCountReq = 5;
    public static int FRDCount = 0;
    public static int FRDCountReq = 4;
    public static int SO1Count = 0;
    public static int SO1CountReq = 3;
    public static int TRDCount = 0;
    public static int TRDCountReq = 5;
    public static int ProjectCount = 0;
    public static int ProjectCountReq = 2;
    public static int DevCount = 0;
    public static int DevCountReq = 20;
    public static int TestingCount = 0;
    public static int TestingCountReq = 15;
    public static int ReleaseCount = 0;
    public static int ReleaseCountReq = 1;
    public static int SO2Count = 0;
    public static int SO2CountReq = 2;

    public static bool BRDComplete = false;
    public static bool FRDComplete = false;
    public static bool SO1Complete = false;
    public static bool TRDComplete = false;
    public static bool ProjectComplete = false;
    public static bool DevComplete = false;
    public static bool TestingComplete = false;
    public static bool ReleaseComplete = false;
    public static bool SO2Complete = false;
    public static bool ENDGAME = false;
    public string Stage = "Standard";
    Text text;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
    }

    public void Reset()
    {
    BRDCount = 0;
    FRDCount = 0;
    SO1Count = 0;
    TRDCount = 0;
    ProjectCount = 0;
    DevCount = 0;
    TestingCount = 0;
    ReleaseCount = 0;
    SO2Count = 0;

        BRDComplete = false;
        FRDComplete = false;
        SO1Complete = false;
        TRDComplete = false;
        ProjectComplete = false;
        DevComplete = false;
        TestingComplete = false;
        ReleaseComplete = false;
        SO2Complete = false;
        ENDGAME = false;
    }
    void Update ()
    {

        Color color = Color.white;
        color.a = 1f;

        if (Stage == "BRD")
        {
            text.text = "Business Requirements: " + BRDCount.ToString() + "/" + BRDCountReq.ToString();
            if (BRDComplete)
            {
                color = Color.gray;
                color.a = 0.8f;
                text.color = color;

            }
            else
            {
                FRDComplete = false;
                SO1Complete = false;
                TRDComplete = false;
                ProjectComplete = false;
                DevComplete = false;
                TestingComplete = false;
                ReleaseComplete = false;
                SO2Complete = false;
            }

        }
        else if (Stage == "FRD")
        {
            text.text = "Functional Requirements: " + FRDCount.ToString() + "/" + FRDCountReq.ToString();

            if (FRDComplete)
            {
                color = Color.gray;
                color.a = 0.8f;
                text.color = color;
            }
            else if ((FRDComplete != true) && (BRDComplete == true))
            {
                color = Color.white;
                color.a = 1f;
                text.color = color;
            }
            else if (BRDComplete != true)
            {
                color.a = 0f;
                text.color = color;
            }

            if (!FRDComplete)
            {
                SO1Complete = false;
                TRDComplete = false;
                ProjectComplete = false;
                DevComplete = false;
                TestingComplete = false;
                ReleaseComplete = false;
                SO2Complete = false;
            }

        }
        else if (Stage == "SO1")
        {
            text.text = "Tech. Sign Off: " + SO1Count.ToString() + "/" + SO1CountReq.ToString();
            if (SO1Complete)
            {
                color = Color.gray;
                color.a = 0.8f;
                text.color = color;
            }
            else if ((SO1Complete != true) && (FRDComplete == true))
            {
                color = Color.white;
                color.a = 1f;
                text.color = color;
            }
            else if (FRDComplete != true)
            {
                color.a = 0f;
                text.color = color;
            }
            else if (FRDComplete == null)
            {
                color = Color.red;
            }

            if (!SO1Complete)
            {
                TRDComplete = false;
                ProjectComplete = false;
                DevComplete = false;
                TestingComplete = false;
                ReleaseComplete = false;
                SO2Complete = false;
            }
        }
        else if (Stage == "TRD")
        {
            text.text = "Technical Design: " + TRDCount.ToString() + "/" + TRDCountReq.ToString();
            if (TRDComplete)
            {
                color = Color.gray;
                color.a = 0.8f;
                text.color = color;
            }
            else if ((TRDComplete != true) && (SO1Complete == true))
            {
                color = Color.white;
                color.a = 1f;
                text.color = color;
            }
            else if (SO1Complete != true)
            {
                color.a = 0f;
                text.color = color;
            }

            if (!TRDComplete)
            {
               
                ProjectComplete = false;
                DevComplete = false;
                TestingComplete = false;
                ReleaseComplete = false;
                SO2Complete = false;
            }
        }
        else if (Stage == "Project")
        {
            text.text = "Project Plan: " + ProjectCount.ToString() + "/" + ProjectCountReq.ToString();
            if (ProjectComplete)
            {
                color = Color.gray;
                color.a = 0.8f;
                text.color = color;
            }
            else if ((ProjectComplete != true) && (TRDComplete == true))
            {
                color = Color.white;
                color.a = 1f;
                text.color = color;
            }
            else if (TRDComplete != true)
            {
                color.a = 0f;
                text.color = color;
            }

            if (!ProjectComplete)
            {
                 DevComplete = false;
                TestingComplete = false;
                ReleaseComplete = false;
                SO2Complete = false;
            }
        }
        else if (Stage == "DEV")
        {
            text.text = "Development: " + DevCount.ToString() + "/" + DevCountReq.ToString();
            if (DevComplete)
            {
                color = Color.gray;
                color.a = 0.8f;
                text.color = color;
            }
            else if ((DevComplete != true) && (ProjectComplete == true))
            {
                color = Color.white;
                color.a = 1f;
                text.color = color;
            }
            else if (ProjectComplete != true)
            {
                color.a = 0f;
                text.color = color;
            }

            if (!DevComplete)
            {
                TestingComplete = false;
                ReleaseComplete = false;
                SO2Complete = false;
            }
        }
        else if (Stage == "Testing")
        {
            text.text = "Testing: " + TestingCount.ToString() + "/" + TestingCountReq.ToString();
            if (TestingComplete)
            {
                color = Color.gray;
                color.a = 0.8f;
                text.color = color;
            }
            else if ((TestingComplete != true) && (DevComplete == true))
            {
                color = Color.white;
                color.a = 1f;
                text.color = color;
            }
            else if (DevComplete != true)
            {
                color.a = 0f;
                text.color = color;
            }

            if (!TestingComplete)
            {
               
                ReleaseComplete = false;
                SO2Complete = false;
            }
        }
        else if (Stage == "Release")
        {
            text.text = "Release: " + ReleaseCount.ToString() + "/" + ReleaseCountReq.ToString();
            if (ReleaseComplete)
            {
                color = Color.gray;
                color.a = 0.8f;
                text.color = color;
            }
            else if ((ReleaseComplete != true) && (TestingComplete == true))
            {
                color = Color.white;
                color.a = 1f;
                text.color = color;
            }
            else if (TestingComplete != true)
            {
                color.a = 0f;
                text.color = color;
            }

            if (!ReleaseComplete)
            {
                SO2Complete = false;
            }
        }
        else if (Stage == "SO2")
        {
            text.text = "Lessons Learned: " + SO2Count.ToString() + "/" + SO2CountReq.ToString();
            if (SO2Complete)
            {
                color = Color.gray;
                color.a = 0.8f;
                text.color = color;

                Application.LoadLevel("ProjectComplete");
            }
            else if ((SO2Complete != true) && (ReleaseComplete == true))
            {
                color = Color.white;
                color.a = 1f;
                text.color = color;
            }
            else if (ReleaseComplete != true)
            {
                color.a = 0f;
                text.color = color;
            }
        }
        else
        {
            text.text = "Score: " + score;
        }

    }
}
