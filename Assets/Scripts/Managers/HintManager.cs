﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class HintManager : MonoBehaviour {
    Text text;
  
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        text = GetComponent<Text>();

        if (ScoreManager.BRDComplete != true)
        {

            text.text = "HINT: Those rascally RABBITS!";
        }
        else if (ScoreManager.FRDComplete != true)
        {
            text.text = "HINT: My manager is such a BEAR!";
        }
        else if (ScoreManager.SO1Complete != true)
        {
            text.text = "HINT: Those HELLEPHANTS will never sign off!";
        }
       
        else if (ScoreManager.TRDComplete != true)
        {
            text.text = "HINT: Has anyone seen the WHITEBOARD markers?";
        }
        else if (ScoreManager.ProjectComplete != true)
        {
            text.text = "HINT: Save the trees! Stop using the PRINTERS!";
        }
        else if (ScoreManager.DevComplete != true)
        {
            text.text = "HINT: But I want duel screen MONITORS too!";
        }
        else if (ScoreManager.TestingComplete != true)
        {
            text.text = "HINT: Must have filed that test plan the the FILE CABINIT!";
        }
        else if (ScoreManager.ReleaseComplete != true)
        {
            text.text = "Someone give me access to the SERVER!";
        }
        else if (ScoreManager.SO2Complete != true)
        {
            text.text = "HINT: RED HELLEPHANTS are scary!";
        }
        else
        {
            text.text = "";
        }
	}
}
