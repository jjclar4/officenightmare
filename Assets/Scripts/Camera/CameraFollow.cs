﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public Transform target;
    public float smoothing = 5f;
    public float negOff = -1f;
    Vector3 offset;
  static Vector3 CamOffsetPos = new Vector3(0f, 0f, 0f);
   // static Vector3 CamMainPos = new Vector3(-50f,15f,25f);
    static Vector3 Cam3Pos = new Vector3(-0f, 0f, -50f);
    static Vector3 Cam4Pos = new Vector3(25f, 0f, -25f);
    static Vector3 Cam2Pos = new Vector3(-25f, 0f, -25f);
    static Vector3 CamResetPos = new Vector3(0f, 0f, 0f);
    static Quaternion CamMainQuat = Quaternion.Euler(30, 0, 0);
    static Quaternion Cam3Quat = Quaternion.Euler(30, 180, 0);
    static Quaternion Cam2Quat = Quaternion.Euler(30, 270, 0);
    static Quaternion Cam4Quat = Quaternion.Euler(30, 90, 0);
    ScoreManager scoreManager = new ScoreManager();
    public int ActiveCameraID = 1;

    void Start()
    {
     
        offset = transform.position - target.position;
        transform.position = target.position;
       
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
            else
            {

                Time.timeScale = 1;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {

            scoreManager.Reset();

            Application.LoadLevel("OfficeNightmare_01");
        }
    }

    void FixedUpdate()
    {
        
        Vector3 targetCamPos = target.position + offset;

        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (ActiveCameraID == 1)
            {
                CamOffsetPos = Cam2Pos;
                transform.rotation = Cam2Quat;
                ActiveCameraID = 2;
                PlayerMovement.CameraID = 2;
            }
            else if (ActiveCameraID == 2)
            {
                CamOffsetPos = Cam3Pos;
                transform.rotation = Cam3Quat;
                ActiveCameraID = 3;
                PlayerMovement.CameraID = 3;
            }
            else if (ActiveCameraID == 3)
            {
                CamOffsetPos = Cam4Pos;
                transform.rotation = Cam4Quat;
                ActiveCameraID = 4;
                PlayerMovement.CameraID = 4;
            }
            else if (ActiveCameraID == 4)
            {
                CamOffsetPos = CamResetPos;
                transform.rotation = CamMainQuat;
                ActiveCameraID = 1;
                PlayerMovement.CameraID = 1;
            }
        }
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (ActiveCameraID == 1)
                {
                    CamOffsetPos = Cam4Pos;
                    transform.rotation = Cam4Quat;
                    ActiveCameraID = 4;
                    PlayerMovement.CameraID = 4;
                }
                else if (ActiveCameraID == 4)
                {
                    CamOffsetPos = Cam3Pos;
                    transform.rotation = Cam3Quat;
                    ActiveCameraID = 3;
                    PlayerMovement.CameraID = 3;
                }
                else if (ActiveCameraID == 3)
                {
                    CamOffsetPos = Cam2Pos;
                    transform.rotation = Cam2Quat;
                    ActiveCameraID = 2;
                    PlayerMovement.CameraID = 2;
                }
                else if (ActiveCameraID == 2)
                {
                    CamOffsetPos = CamResetPos;
                    transform.rotation = CamMainQuat;
                    ActiveCameraID = 1;
                    PlayerMovement.CameraID = 1;
                }
            }
        
            transform.position = Vector3.Lerp(transform.position, targetCamPos - CamOffsetPos, smoothing + Time.deltaTime);
        
     
      
       
        
    }
}
