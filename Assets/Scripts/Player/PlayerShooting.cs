﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;
    //static Vector3 TrajectOffset = new Vector3(-0f, 0.2f, -0f);

    float timer;
    Ray shootRay;
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {
        timer += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot ();
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
    {
        timer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;
        bool takeDamage = true;

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();


            if ((enemyHealth.EnemyType == "TRD") && (ScoreManager.SO1Complete != true))
            {
                takeDamage = false;
            }
            else if ((enemyHealth.EnemyType == "Project") && (ScoreManager.TRDComplete != true))
            {
                takeDamage = false;
            }

            else if ((enemyHealth.EnemyType == "Dev") && (ScoreManager.ProjectComplete != true))
            {
                takeDamage = false;
            }
            else if ((enemyHealth.EnemyType == "Testing") && (ScoreManager.DevComplete != true))
            {
                takeDamage = false;
            }
            else if ((enemyHealth.EnemyType == "Release") && (ScoreManager.TestingComplete != true))
            {
                takeDamage = false;
            }
            if ((enemyHealth != null)&&(takeDamage == true))
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }
}
