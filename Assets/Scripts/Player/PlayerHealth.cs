﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);

    private bool playAudio = false, someRandomCondition = false;
    private float currentTime = 0.0f, executedTime = 0.0f;
    public float timeToWait = 4.0f, timeBetweenTexts = 5.0f, percentToAct = 50.0f;
    public List<AudioClip> AudioList = new List<AudioClip>();
    public AudioClip defaultAudio = new AudioClip();
    Animator anim;
    AudioSource playerAudio;
    PlayerMovement playerMovement;
    PlayerShooting playerShooting;
    bool isDead;
    bool damaged;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        playerAudio = GetComponent <AudioSource> ();
        playerMovement = GetComponent <PlayerMovement> ();
        playerShooting = GetComponentInChildren <PlayerShooting> ();
        currentHealth = startingHealth;
    }


    void Update ()
    {
        if(damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;


        if (ScoreManager.ENDGAME)
        {
            playAudio = false;
        }
        else
        {
            currentTime = Time.time;

            if (!playAudio)
            {
                if (currentTime - executedTime > timeBetweenTexts)
                {
                    if (Random.Range(0, 100) < percentToAct)
                    {

                        playerAudio.clip = AudioList[Random.Range(0, AudioList.Count)];
                        playerAudio.Play();
                        

                        someRandomCondition = true;
                    }
                    executedTime = Time.time;
                }
            }
            else
            {
                if (executedTime != 0.0f)
                {
                    if (currentTime - executedTime > timeToWait)
                    {
                        //executedTime = 0.0f;
                        someRandomCondition = false;
                    }
                }
            }


            if (someRandomCondition)
            {
                playAudio = true;
            }
            else
            {
                playAudio = false;
            }
        }

    }


    public void TakeDamage (int amount)
    {
        damaged = true;
        playerAudio.clip = defaultAudio;
        currentHealth -= amount;

        healthSlider.value = currentHealth;

        playerAudio.Play ();

        if(currentHealth <= 0 && !isDead)
        {
            Death ();
        }
    }


    void Death ()
    {
        isDead = true;

        playerShooting.DisableEffects ();

        anim.SetTrigger ("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play ();

        Application.LoadLevel("GameOver");
        playerMovement.enabled = false;
        playerShooting.enabled = false;
    }


    public void RestartLevel ()
    {
        Application.LoadLevel (Application.loadedLevel);
    }
}
