﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip deathClip;
    public Transform Player;
    public string EnemyType = "Standard";

    Animator anim;
    AudioSource enemyAudio;
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;
    bool isDead;
    bool isSinking;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        enemyAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren <ParticleSystem> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();

        currentHealth = startingHealth;
    }


    void Update ()
    {
        if(isSinking)
        {
            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }


    public void TakeDamage (int amount, Vector3 hitPoint)
    {
        if(isDead)
            return;
        bool canTakeDamage = true;

        if (canTakeDamage) { 
        enemyAudio.Play ();

        currentHealth -= amount;
            
        hitParticles.transform.position = hitPoint;
        hitParticles.Play();

        if(currentHealth <= 0)
        {
            Death ();
        }
        }
    }


    void Death ()
    {
        isDead = true;

        capsuleCollider.isTrigger = true;

        if (EnemyType == "BRD")
        {
            if (ScoreManager.BRDCount < ScoreManager.BRDCountReq) { 
                ScoreManager.BRDCount += 1;
            }
            if (ScoreManager.BRDCount >= ScoreManager.BRDCountReq)
            {
                ScoreManager.BRDComplete = true;
            }

        }
        else if ((EnemyType == "FRD") && (ScoreManager.BRDComplete))
        {
            if (ScoreManager.FRDCount < ScoreManager.FRDCountReq)
            {
                ScoreManager.FRDCount += 1;
            }
            if (ScoreManager.FRDCount >= ScoreManager.FRDCountReq)
            {
                ScoreManager.FRDComplete = true;
            }

        }
        else if ((EnemyType == "SO1") && (ScoreManager.FRDComplete))
        {
            if (ScoreManager.SO1Count < ScoreManager.SO1CountReq)
            {
                ScoreManager.SO1Count += 1;
            }
            if (ScoreManager.SO1Count >= ScoreManager.SO1CountReq)
            {
                ScoreManager.SO1Complete = true;
            }

        }
        else if (EnemyType == "TRD")
        {
            if (ScoreManager.TRDCount < ScoreManager.TRDCountReq)
            {
                ScoreManager.TRDCount += 1;
            }
            if (ScoreManager.TRDCount >= ScoreManager.TRDCountReq)
            {
                ScoreManager.TRDComplete = true;
            }

        }
        else if (EnemyType == "Project")
        {
            if (ScoreManager.ProjectCount < ScoreManager.ProjectCountReq)
            {
                ScoreManager.ProjectCount += 1;
            }
            if (ScoreManager.ProjectCount >= ScoreManager.ProjectCountReq)
            {
                ScoreManager.ProjectComplete = true;
            }

        }
        else if (EnemyType == "Dev")
        {
            if (ScoreManager.DevCount < ScoreManager.DevCountReq)
            {
                ScoreManager.DevCount += 1;
            }
            if (ScoreManager.DevCount >= ScoreManager.DevCountReq)
            {
                ScoreManager.DevComplete = true;
            }

        }
        else if (EnemyType == "Testing")
        {
            if (ScoreManager.TestingCount < ScoreManager.TestingCountReq)
            {
                ScoreManager.TestingCount += 1;
            }
            if (ScoreManager.TestingCount >= ScoreManager.TestingCountReq)
            {
                ScoreManager.TestingComplete = true;
            }

        }
        else if (EnemyType == "Release")
        {
            if (ScoreManager.ReleaseCount < ScoreManager.ReleaseCountReq)
            {
                ScoreManager.ReleaseCount += 1;
            }
            if (ScoreManager.ReleaseCount >= ScoreManager.ReleaseCountReq)
            {
                ScoreManager.ReleaseComplete = true;
            }

        }
        else if ((EnemyType == "SO2") && (ScoreManager.ReleaseComplete))
        {
            if (ScoreManager.SO2Count < ScoreManager.SO2CountReq)
            {
                ScoreManager.SO2Count += 1;
            }
            if (ScoreManager.SO2Count >= ScoreManager.SO2CountReq)
            {
                ScoreManager.SO2Complete = true;
            }

        }
        anim.SetTrigger ("Dead");

        enemyAudio.clip = deathClip;
        enemyAudio.Play ();
    }


    public void StartSinking()
    {
        //delayed distroy time
        List<string> DelayObjsDistroy = new List<string>();

        DelayObjsDistroy.Add("BRD");
        DelayObjsDistroy.Add("FRD");
        DelayObjsDistroy.Add("SO2");

        try
        {
            GetComponent<NavMeshAgent>().enabled = false;
        }
        catch
        {

        }
        GetComponent<Rigidbody>().isKinematic = true;
        isSinking = true;
        ScoreManager.score += scoreValue;

        if (DelayObjsDistroy.Contains(EnemyType)) {
            Destroy (gameObject, 2f);
        }
        else { 
        Destroy(gameObject, 0f);
        }
    }
}
